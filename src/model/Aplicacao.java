/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Aplicacao
{
    private MateriaPrima m_oMateriaPrima;
    private Produto m_oProdutoResultante;
    private double m_dRacio;

    /**
     * @return the m_oMateriaPrima
     */
    public MateriaPrima getMateriaPrima()
    {
        return m_oMateriaPrima;
    }

    /**
     * @param oMateriaPrima the m_oMateriaPrima to set
     */
    public void setMateriaPrima(MateriaPrima oMateriaPrima)
    {
        this.m_oMateriaPrima = oMateriaPrima;
    }

    /**
     * @return the m_oProdutoResultante
     */
    public Produto getProdutoResultante()
    {
        return m_oProdutoResultante;
    }

    /**
     * @param oProdutoResultante the m_oProdutoResultante to set
     */
    public void setProdutoResultante(Produto oProdutoResultante)
    {
        this.m_oProdutoResultante = oProdutoResultante;
    }

    /**
     * @return the m_dRacio
     */
    public double getRacio()
    {
        return m_dRacio;
    }

    /**
     * @param dRacio the m_dRacio to set
     */
    public void setRacio(double dRacio)
    {
        this.m_dRacio = dRacio;
    }

    boolean valida()
    {
        // Escrever aqui o código de validação
        
        //
        return true; 
    }
    
    @Override
    public String toString()
    {
        return this.m_oMateriaPrima.getID() + ";" + this.m_oProdutoResultante.getID() + ";" + this.m_dRacio + ";";
    }
    
}
