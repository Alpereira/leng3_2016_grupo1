/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Servico
{
    private String m_sCodigo;
    private String m_sDescricao;
    private FichaTecnica m_oFichaTecnica;
    private Disponibilizacao m_oDisponibilizacao;
    private PotencialDisponibilizacao m_oPotencialDisponibilizacao;
    private final List<Aplicacao> m_lstAplicacoes;
    private String m_sOutroLocal ;
//    private Disponibilizacao m_sCusto;
//    private Disponibilizacao m_sCapacidadeNominal;
    
    public Servico()
    {
        this.m_lstAplicacoes = new ArrayList<>();
    }

    /**
     * @return the m_sCodigo
     */
    public String getCodigo()
    {
        return m_sCodigo;
    }

    /**
     * @param sCodigo the m_sCodigo to set
     */
    public void setCodigo(String sCodigo)
    {
        this.m_sCodigo = sCodigo;
    }

    /**
     * @return the m_sDescricao
     */
    public String getDescricao()
    {
        return m_sDescricao;
    }

    /**
     * @param sDescricao the msDescricao to set
     */
    public void setDescricao(String sDescricao)
    {
        this.m_sDescricao = sDescricao;
    }

    
    public FichaTecnica getFichaTecnica()
    {
        return this.m_oFichaTecnica;
    }
    
    public void setFichaTecnica(String sTexto)
    {
        this.m_oFichaTecnica = new FichaTecnica();
        this.m_oFichaTecnica.setDados(sTexto);
    }
    /**
     * @return the m_sLocalEspecializado
     */
    public String getM_sOutroLocal() {
        return m_sOutroLocal;
    }
/**
     * @param m_sOutroLocal the m_sOutroLocal to set
     */
    public void setM_sOutroLocal(String m_sOutroLocal) {
        this.m_sOutroLocal = m_sOutroLocal;
    }

    /**
     * @return the m_oDisponibilizacao
     */
    public Disponibilizacao getM_oDisponibilizacao() {
        return this.m_oDisponibilizacao;
    }

    /**
     * @param m_oDisponibilizacao the m_oDisponibilizacao to set
     */
    public void setM_oDisponibilizacao(String sCusto, String sCapacidadeNominal) {
        this.m_oDisponibilizacao = new Disponibilizacao();
        this.m_oDisponibilizacao.setM_sCusto(sCusto);
        this.m_oDisponibilizacao.setM_sCapacidadeNominal(sCapacidadeNominal);
        
        
    }

    /**
     * @return the m_oPotencialDisponibilizacao
     */
    public PotencialDisponibilizacao getM_oPotencialDisponibilizacao() {
        return this.m_oPotencialDisponibilizacao;
    }

    /**
     * @param m_oPotencialDisponibilizacao the m_oPotencialDisponibilizacao to set
     */
    public void setM_oPotencialDisponibilizacao(String sCustoAbertura) {
        this.m_oPotencialDisponibilizacao = new PotencialDisponibilizacao();
        this.m_oPotencialDisponibilizacao.setM_sCustoAbertura(sCustoAbertura);
        
    }

     /**
     * @return the m_sCusto
     */
//    public String getM_sCusto() {
//        return m_sCusto;
//    }
//
//    /**
//     * @param m_sCusto the m_sCusto to set
//     */
//    public void setM_sCusto(String m_sCusto) {
//        this.m_sCusto = m_sCusto;
//    }
//
//    /**
//     * @return the m_sCapacidadeNominal
//     */
//    public String getM_sCapacidadeNominal() {
//        return m_sCapacidadeNominal;
//    }
//
//    /**
//     * @param m_sCapacidadeNominal the m_sCapacidadeNominal to set
//     */
//    public void setM_sCapacidadeNominal(String m_sCapacidadeNominal) {
//        this.m_sCapacidadeNominal = m_sCapacidadeNominal;
//    }
//    
    boolean valida()
    {
        return true;
    }

    boolean isIdentifiableAs(String sCodigo)
    {
        return this.m_sCodigo.equals(sCodigo);
    }
    
    @Override
    public String toString()
    {
        String sApls = aplicacoestoString();
        return String.format("%s;%s\n%s",this.m_sCodigo, this.m_sDescricao, this.getM_sOutroLocal(),sApls);
        
    }

    public Aplicacao novaAplicacao()
    {
        return new Aplicacao();
    }

    public boolean guardaAplicacao(Aplicacao oApl)
    {
        if (this.validaAplicacao(oApl))
            return this.m_lstAplicacoes.add(oApl);
        return false;
    }

    private boolean validaAplicacao(Aplicacao oApl)
    {
        boolean bRet = false;
        if (oApl.valida())
        {
           // Escrever aqui o código de validação
        
           //
           bRet = true; 
        }
        return bRet;
    }

    private String aplicacoestoString()
    {
        String sRet = "Aplicações:";
        for (Aplicacao apl : this.m_lstAplicacoes)
        {
            sRet += String.format("\n%s",apl.toString());
        }
        return sRet;
    }

}
