/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Produto
{
    private String m_sID;
    private String m_sDescBreve;
    private String m_sDescCompleta;
    private String m_sCodigoAlfandegario;

    /**
     * @return the m_sID
     */
    public String getID()
    {
        return m_sID;
    }

    /**
     * @return the m_sDescBreve
     */
    public String getDescricaoBreve()
    {
        return m_sDescBreve;
    }

    /**
     * @return the m_sDescCompleta
     */
    public String getDescricaoCompleta()
    {
        return m_sDescCompleta;
    }

    /**
     * @return the m_sCodigoAlfandegario
     */
    public String getCodigoAlfandegario()
    {
        return m_sCodigoAlfandegario;
    }

    /**
     * @param m_sID the m_sID to set
     */
    public void setID(String m_sID)
    {
        this.m_sID = m_sID;
    }

    /**
     * @param m_sDescBreve the m_sDescBreve to set
     */
    public void setDescricaoBreve(String m_sDescBreve)
    {
        this.m_sDescBreve = m_sDescBreve;
    }

    /**
     * @param m_sDescCompleta the m_sDescCompleta to set
     */
    public void setDescricaoCompleta(String m_sDescCompleta)
    {
        this.m_sDescCompleta = m_sDescCompleta;
    }

    /**
     * @param m_sCodigoAlfandegario the m_sCodigoAlfandegario to set
     */
    public void setCodigoAlfandegario(String m_sCodigoAlfandegario)
    {
        this.m_sCodigoAlfandegario = m_sCodigoAlfandegario;
    }

    public boolean isIdentifiableAs(String sID)
    {
        return this.m_sID.equals(sID);
    }

    public boolean valida()
    {
        // Escrever aqui o código de validação
        
        //
        return true;
    }
    
    @Override
    public String toString()
    {
        return this.m_sID + ";" + this.m_sDescBreve + ";" + this.m_sDescCompleta + ";" + this.m_sCodigoAlfandegario +";";
    }
}
