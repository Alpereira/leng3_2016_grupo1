/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Utilizador
 */
public class RegistoServico {
    private final List<Servico> m_lstServicos;
    
public RegistoServico() {
    this.m_lstServicos = new ArrayList<>();

}

    /* Servicos */
    public Servico novoServico()
    {
        return new Servico();
    }
    
    public boolean validaServico(Servico srv)
    {
        boolean bRet = false;
        if (srv.valida())
        {
           // Escrever aqui o código de validação
        
           //
           bRet = true; 
        }
        return bRet;
    }
    
    public boolean registaServico(Servico srv)
    {
        if (this.validaServico(srv))
        {
           return addServico(srv);
        }
        return false;
    }
    
    private boolean addServico(Servico srv)
    {
        return m_lstServicos.add(srv);
    }
    
    public List<Servico> getListaServicos()
    {
        return this.m_lstServicos;
    }

    public Servico getServico(String sCodigo)
    {
        for(Servico srv : this.m_lstServicos)
        {
            if (srv.isIdentifiableAs(sCodigo))
            {
                return srv;
            }
        }
        
        return null;
    }


}