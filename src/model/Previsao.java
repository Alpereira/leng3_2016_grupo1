/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Previsao
{
    private LocalProducao m_oLocalProducao;
    private double m_dQuantidade; 
    
    public Previsao(LocalProducao local)
    {
        this.m_oLocalProducao = local;
    }

    /**
     * @return the m_dQuantidade
     */
    public double getQuantidade()
    {
        return m_dQuantidade;
    }

    /**
     * @param dQuantidade the m_dQuantidade to set
     */
    public void setQuantidade(double dQuantidade)
    {
        this.m_dQuantidade = dQuantidade;
    }
    
    boolean valida()
    {
        // Escrever aqui o código de validação
        
        //
        return true;
    }
    
    @Override
    public String toString()
    {
        return String.format("%s;%f\n",this.m_oLocalProducao.toString(), this.m_dQuantidade);
        
    }

    boolean isAbout(LocalProducao local)
    {
        return this.m_oLocalProducao.equals(local);
    }
}
