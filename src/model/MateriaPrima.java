/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class MateriaPrima extends Produto
{
    
    private final List<PeriodoColheita> m_lstPeriodosColheita;

    public MateriaPrima()
    {
        super();
        this.m_lstPeriodosColheita = new ArrayList<>();
    }
    
    public PeriodoColheita novoPeriodoColheita(Date dtIni, Date dtFim)
    {
        
        PeriodoColheita pc = new PeriodoColheita();
        pc.setDataInicio(dtIni);
        pc.setDataFim(dtFim);
        
        return pc;
    }
    
    public boolean validaPeriodoColheita(PeriodoColheita pc)
    {
        boolean bRet = false;
        if (pc.valida())
        {
            // Escrever aqui validações globais
            
            //
            
            bRet = true;
        }
        return bRet;
    }
    
    public boolean registaPeriodoColheita(PeriodoColheita pc)
    {
        boolean bRet = validaPeriodoColheita(pc);
        if (bRet)
        {
            bRet = this.m_lstPeriodosColheita.add(pc);
        }
        return bRet;
    }
    
    public List<PeriodoColheita> getPeriodosSemPrevisao(LocalProducao local)
    {
        List<PeriodoColheita> lst = new ArrayList<>();
        for(PeriodoColheita pc: this.m_lstPeriodosColheita)
        {
            if (!pc.hasPrevisao(local))
                lst.add(pc);
        }
        
        return lst;
    }
}
