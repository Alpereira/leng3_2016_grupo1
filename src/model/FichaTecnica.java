/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class FichaTecnica
{
    private String m_sTexto;
    
    public FichaTecnica()
    {
    }

    /**
     * @return the m_sTexto
     */
    public String getDados()
    {
        return m_sTexto;
    }

    /**
     * @param m_sTexto the m_sTexto to set
     */
    public void setDados(String m_sTexto)
    {
        this.m_sTexto = m_sTexto;
    }
    
    
}
