/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class LocalProducao
{
    private int m_iID;
    private String m_sDenominacao;
    private String m_sAbreviatura;
    private String m_sEndereco;
    private String m_sCoordGPS;
    private List<MateriaPrima> m_lstMatPrimas;
    
    public LocalProducao()
    {
        this.m_lstMatPrimas = new ArrayList<>();
    }
    
    public LocalProducao(int ID, String sDenominacao,String sAbreviatura,String sEndereco,String sCoordGPS)
    {
        this.setID(ID);
        this.setDenominacao(sDenominacao);
        this.setAbreviatura(sAbreviatura);
        this.setEndereco(sEndereco);
        this.setCoordGPS(sCoordGPS);
        this.m_lstMatPrimas = new ArrayList<>();
    }
    public final void setID(int iID)
    {
        this.m_iID = iID;
    }
    
    public final void setDenominacao(String sDenominacao)
    {
        this.m_sDenominacao = sDenominacao;
    }
    
    public final void setAbreviatura(String sAbreviatura)
    {
        this.m_sAbreviatura = sAbreviatura;
    }
    
    public final void setEndereco(String sEndereco)
    {
        this.m_sEndereco = sEndereco;
    }
    
    public final void setCoordGPS(String sCoordGPS)
    {
        this.m_sCoordGPS = sCoordGPS;
    }
    
    public boolean valida()
    {
        // Escrever aqui o código de validação
        
        //
        return true;
    }
    
    
    public boolean isIdentifiableAs(int iID)
    {
        return this.m_iID==iID;
    }
    
    @Override
    public String toString()
    {
        return this.m_sAbreviatura + ";" + this.m_sDenominacao + ";" + this.m_sEndereco + ";" + this.m_sCoordGPS +";";
    }
    
    public List<MateriaPrima> getMateriasPrimasProduzidas()
    {
        return m_lstMatPrimas;
    }
    
    public void addMateriaPrimaProduzida(MateriaPrima mp)
    {
        this.m_lstMatPrimas.add(mp);
    }
}
