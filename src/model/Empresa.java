/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Empresa
{
    private final List<LocalProducao> m_lstLocaisProducao;
    private final List<MateriaPrima> m_lstMateriasPrimas;
    private final List<ProdutoDerivado> m_lstProdDerivados;
    private final List<Servico> m_lstServicos;
    

    public Empresa()
    {
        this.m_lstLocaisProducao = new ArrayList<>();
        this.m_lstMateriasPrimas = new ArrayList<>();
        this.m_lstProdDerivados = new ArrayList<>();
        this.m_lstServicos = new ArrayList<>();
        
        fillInData();
    }
    
    private void fillInData()
    {
        // Dados de Teste
        //Preenche alguns Locais de Produção
        for(Integer i=1;i<=4;i++)
            addLocalProducao(new LocalProducao(i,"Local Prod.: " + i.toString(),"LP"+i.toString(),"",""));
        
        //Preencher outros dados aqui
        
    }
    
    /* Locais de Produção */
    
    public LocalProducao novoLocalProducao()
    {
        return new LocalProducao();
    }
    
    public boolean validaLocalProducao(LocalProducao local)
    {
        boolean bRet = false;
        if (local.valida())
        {
           // Escrever aqui o código de validação
        
           //
           bRet = true; 
        }
        return bRet;
    }
    
    public boolean registaLocalProducao(LocalProducao local)
    {
        if (this.validaLocalProducao(local))
        {
           adiconaTodasMateriaPrimaALocal(local);
           return addLocalProducao(local);
        }
        return false;
    }
    
    private boolean addLocalProducao(LocalProducao local)
    {
        return m_lstLocaisProducao.add(local);
    }
    
    public List<LocalProducao> getListaLocaisProducao()
    {
        return this.m_lstLocaisProducao;
    }

    public LocalProducao getLocalProducao(int iID)
    {
        for(LocalProducao local : this.m_lstLocaisProducao)
        {
            if (local.isIdentifiableAs(iID))
            {
                return local;
            }
        }
        
        return null;
    }
    
    
    /* Matérias-Primas */
    public MateriaPrima novaMateriaPrima()
    {
        return new MateriaPrima();
    }
    
    public boolean validaMateriaPrima(MateriaPrima mp)
    {
        boolean bRet = false;
        if (mp.valida())
        {
           // Escrever aqui o código de validação
        
           //
           bRet = true; 
        }
        return bRet;
    }
    
    public boolean registaMateriaPrima(MateriaPrima mp)
    {
        if (this.validaMateriaPrima(mp))
        {
           return addMateriaPrima(mp);
        }
        return false;
    }
    
    private boolean addMateriaPrima(MateriaPrima mp)
    {
        boolean bRet = m_lstMateriasPrimas.add(mp);
        adiconaMateriaPrimaTodosLocais(mp);
        return bRet;
    }
    
    public List<MateriaPrima> getListaMateriasPrimas()
    {
        return this.m_lstMateriasPrimas;
    }

    public MateriaPrima getMateriaPrima(String sID)
    {
        for(MateriaPrima mp : this.m_lstMateriasPrimas)
        {
            if (mp.isIdentifiableAs(sID))
            {
                return mp;
            }
        }
        
        return null;
    }
    
    /* Produtos Derivados */
    public ProdutoDerivado novoProdutoDerivado()
    {
        return new ProdutoDerivado();
    }
    
    public boolean validaProdutoDerivado(ProdutoDerivado pd)
    {
        boolean bRet = false;
        if (pd.valida())
        {
           // Escrever aqui o código de validação
        
           //
           bRet = true; 
        }
        return bRet;
    }
    
    public boolean registaProdutoDerivado(ProdutoDerivado pd)
    {
        if (this.validaProdutoDerivado(pd))
        {
           return addProdutoDerivado(pd);
        }
        return false;
    }
    
    private boolean addProdutoDerivado(ProdutoDerivado pd)
    {
        return m_lstProdDerivados.add(pd);
    }
    
    public List<ProdutoDerivado> getListaProdutosDerivados()
    {
        return this.m_lstProdDerivados;
    }

    public ProdutoDerivado getProdutoDerivado(String sID)
    {
        for(ProdutoDerivado pd : this.m_lstProdDerivados)
        {
            if (pd.isIdentifiableAs(sID))
            {
                return pd;
            }
        }
        
        return null;
    }
    
    public List<Produto> getProdutos()
    {
        List<Produto> lst = new ArrayList<>();
        lst.addAll(this.m_lstMateriasPrimas);
        lst.addAll(this.m_lstProdDerivados);
        return lst;
    }
    
    /* Servicos */
    public Servico novoServico()
    {
        return new Servico();
    }
    
    public boolean validaServico(Servico srv)
    {
        boolean bRet = false;
        if (srv.valida())
        {
           // Escrever aqui o código de validação
        
           //
           bRet = true; 
        }
        return bRet;
    }
    
    public boolean registaServico(Servico srv)
    {
        if (this.validaServico(srv))
        {
           return addServico(srv);
        }
        return false;
    }
    
    private boolean addServico(Servico srv)
    {
        return m_lstServicos.add(srv);
    }
    
    public List<Servico> getListaServicos()
    {
        return this.m_lstServicos;
    }

    public Servico getServico(String sCodigo)
    {
        for(Servico srv : this.m_lstServicos)
        {
            if (srv.isIdentifiableAs(sCodigo))
            {
                return srv;
            }
        }
        
        return null;
    }
    
    public List<LocalProducao> getLocaisProducao(String user)
    {
        /* Atualmente ainda não se sabe quem é responsavel por um local de produção
           
            Portanto, vai-se ignorar esta questão para já.
        
        */
        return this.m_lstLocaisProducao;
    }
    
    // Usado temporariamente enquanto nenhum UC especificar que MP os Locais Produzem
    private void adiconaMateriaPrimaTodosLocais(MateriaPrima mp)
    {
        for(LocalProducao l: this.m_lstLocaisProducao)
            l.addMateriaPrimaProduzida(mp);
    }
    
    // Usado temporariamente enquanto nenhum UC especificar que MP os Locais Produzem
    private void adiconaTodasMateriaPrimaALocal(LocalProducao local)
    {
        for(MateriaPrima mp: this.m_lstMateriasPrimas)
            local.addMateriaPrimaProduzida(mp);
    }
}
    
    
