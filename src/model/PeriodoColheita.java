/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class PeriodoColheita
{
    private Date m_dtInicio;
    private Date m_dtFim;
    private List<Previsao> m_lstPrevisoes;
    
    public PeriodoColheita()
    {
        this.m_lstPrevisoes = new ArrayList<>();
    }
    /**
     * @return the m_dtInicio
     */
    public Date getDataInicio()
    {
        return m_dtInicio;
    }

    /**
     * @param m_dtInicio the m_dtInicio to set
     */
    public void setDataInicio(Date m_dtInicio)
    {
        this.m_dtInicio = m_dtInicio;
    }

    /**
     * @return the m_dtFim
     */
    public Date getDataFim()
    {
        return m_dtFim;
    }

    /**
     * @param m_dtFim the m_dtFim to set
     */
    public void setDataFim(Date m_dtFim)
    {
        this.m_dtFim = m_dtFim;
    }
    
    public boolean valida()
    {
        // Escrever aqui o código de validação
        
        //
        return true;
    }
    
    
    @Override
    public String toString()
    {
        return this.m_dtInicio.toString() + ";" + this.m_dtFim.toString() ;
    }

    public Previsao novaPrevisao(LocalProducao local)
    {
        return new Previsao(local); 
    }
    
    public boolean guardaPrevisao(Previsao prev)
    {
        if (validaPrevisao(prev))
            return this.m_lstPrevisoes.add(prev);
        return false;
    }
    
    boolean hasPrevisao(LocalProducao local)
    {
        boolean bRet = false;
        for(Previsao prev: this.m_lstPrevisoes)
        {
            if (prev.isAbout(local))
                return true;
        }
        
        return bRet;
    }

    private boolean validaPrevisao(Previsao prev)
    {
        boolean bRet = false;
        if (prev.valida())
        {
            // Escrever aqui validações globais
            
            //
            bRet = true;
        }
        return bRet;
    }
}
