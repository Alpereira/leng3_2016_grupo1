/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import java.util.List;
import model.Aplicacao;
import model.Empresa;
import model.MateriaPrima;
import model.Produto;
import model.Servico;
import model.ProdutoDerivado;
import model.RegistoDisponibilizacao;
import model.RegistoOutroLocal;
import model.RegistoPotencialDisponibilizacao;
import model.RegistoProduto;
import model.RegistoServico;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DefinirServicoController
{
    private final Empresa m_oEmpresa;
    private final RegistoServico m_oRegistoServico;
    private final RegistoProduto m_oRegistoProduto;
    private final RegistoOutroLocal m_oRegistoOutroLocal;
    private final RegistoDisponibilizacao m_oRegistoDisponibilizacao;
    private final RegistoPotencialDisponibilizacao m_oRegistoPotencialDisponibilizacao;
    private Servico m_oServico;
    private Aplicacao m_oApl;
    
    public DefinirServicoController(Empresa oEmpresa,RegistoOutroLocal oRegistoOutroLocal,RegistoServico oRegistoServico,RegistoProduto oRegistoProduto,RegistoDisponibilizacao oRegistoDisponibilizacao, RegistoPotencialDisponibilizacao oRegistoPotencialDisponibilizacao)
    {
        this.m_oEmpresa = oEmpresa;
        this.m_oRegistoServico = oRegistoServico;
        this.m_oRegistoOutroLocal= oRegistoOutroLocal;
        this.m_oRegistoProduto= oRegistoProduto;
        this.m_oRegistoDisponibilizacao= oRegistoDisponibilizacao;
        this.m_oRegistoPotencialDisponibilizacao= oRegistoPotencialDisponibilizacao;
    }
    
    public void novoServico()
    {
        this.m_oServico = this.m_oRegistoServico.novoServico();
    }
    
    public List<Produto> setDados(String sCodigo, String sDescricao,String sFxTec,String sLespec, String sCusto, String sCapacidade, String sCustoAbertura)
    {
        this.m_oServico.setCodigo(sCodigo);
        this.m_oServico.setDescricao(sDescricao);
        this.m_oServico.setFichaTecnica(sFxTec);
        this.m_oServico.setM_sOutroLocal(sLespec);
        this.m_oServico.setM_oDisponibilizacao(sCusto, sCapacidade);
        this.m_oServico.setM_oPotencialDisponibilizacao(sCustoAbertura);
        return this.m_oRegistoProduto.getProdutos();
    }
    
    public void setMateriaPrima(MateriaPrima mp)
    {
        this.m_oApl = this.m_oServico.novaAplicacao();
        this.m_oApl.setMateriaPrima(mp);
    }
    
    public void setProdutoResultante(Produto pr)
    {
        this.m_oApl.setProdutoResultante(pr);
    }
    // set produto derivado
    public void setProdutoDerivado(ProdutoDerivado pd)
    {   this.m_oApl= this.m_oServico.novaAplicacao();
        this.m_oApl.setProdutoDerivado(pd);
    
    }
    public boolean setRacio(double racio)
    {
        this.m_oApl.setRacio(racio);
        
        return this.m_oServico.guardaAplicacao(this.m_oApl);
    }
    
    
    public boolean validaServico()
    {
        return this.m_oRegistoServico.validaServico(this.m_oServico);
    }
    
    public boolean registaServico()
    {
        return this.m_oRegistoServico.registaServico(this.m_oServico);
    }

    public String getServicoAsString()
    {
        return this.m_oServico.toString();
    }
}
