/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import java.util.Date;
import java.util.List;
import model.Empresa;
import model.MateriaPrima;
import model.PeriodoColheita;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DefinirPeriodosColheitaController
{
    private final Empresa m_oEmpresa;
    private MateriaPrima m_oMatPrima;
    public DefinirPeriodosColheitaController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public List<MateriaPrima> getListaMateriasPrimas()
    {
        return this.m_oEmpresa.getListaMateriasPrimas();
    }
    
    public void setMateriaPrima(MateriaPrima mp)
    {
        this.m_oMatPrima = mp;
    }
    
    public boolean addPeriodoColheita(Date dtIni, Date dtFim)
    {
        boolean bRet = false;
        
        PeriodoColheita pc = this.m_oMatPrima.novoPeriodoColheita(dtIni, dtFim);
        if (this.m_oMatPrima.validaPeriodoColheita(pc))
        {
            bRet = this.m_oMatPrima.registaPeriodoColheita(pc);
        }
        
        
        return bRet;
    }
    
    
}
