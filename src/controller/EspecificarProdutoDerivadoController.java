/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import model.Empresa;
import model.ProdutoDerivado;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EspecificarProdutoDerivadoController
{
    private final Empresa m_oEmpresa;
    private ProdutoDerivado m_oProdutoDerivado;
    public EspecificarProdutoDerivadoController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public void novoProdutoDerivado()
    {
        this.m_oProdutoDerivado = this.m_oEmpresa.novoProdutoDerivado();
    }
    
    public void setDados(String sID, String sDescBreve,String sDescCompleta,String sCodAlfandegario)
    {
        this.m_oProdutoDerivado.setID(sID);
        this.m_oProdutoDerivado.setDescricaoBreve(sDescBreve);
        this.m_oProdutoDerivado.setDescricaoCompleta(sDescCompleta);
        this.m_oProdutoDerivado.setCodigoAlfandegario(sCodAlfandegario);
    }
    
    public boolean registaProdutoDerivado()
    {
        return this.m_oEmpresa.registaProdutoDerivado(this.m_oProdutoDerivado);
    }

    public String getProdutoDerivadoAsString()
    {
        return this.m_oProdutoDerivado.toString();
    }
}
