/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import java.util.List;
import model.Empresa;
import model.LocalProducao;
import model.MateriaPrima;
import model.PeriodoColheita;
import model.Previsao;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EfetuarPrevisaoProducaoController
{
    private final Empresa m_oEmpresa;
    private LocalProducao m_oLocal;
    private MateriaPrima m_oMatPrima;
    public EfetuarPrevisaoProducaoController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public List<LocalProducao> getLocaisProducao(String user)
    {
        return this.m_oEmpresa.getLocaisProducao(user);
    }
    
    public List<MateriaPrima> getMateriaPrimasLocProd(LocalProducao local)
    {
        this.m_oLocal = local;
        return local.getMateriasPrimasProduzidas();
    }
    
    public List<PeriodoColheita> getPeriodosColheitaSemPrevisao(MateriaPrima mp)
    {
        this.m_oMatPrima = mp;
        return mp.getPeriodosSemPrevisao(this.m_oLocal);
    }
    
    public void setMateriaPrima(MateriaPrima mp)
    {
        this.m_oMatPrima = mp;
    }
    
    public boolean addPrevisao(PeriodoColheita pc, double qtd)
    {
        
        Previsao prev =  pc.novaPrevisao(m_oLocal);
        prev.setQuantidade(qtd);
       
       return pc.guardaPrevisao(prev);
   
    }
}
