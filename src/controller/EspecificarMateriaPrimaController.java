/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import model.Empresa;
import model.MateriaPrima;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EspecificarMateriaPrimaController
{
    private final Empresa m_oEmpresa;
    private MateriaPrima m_oMateriaPrima;
    public EspecificarMateriaPrimaController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public void novaMateriaPrima()
    {
        this.m_oMateriaPrima = this.m_oEmpresa.novaMateriaPrima();
    }
    
    public void setDados(String sID, String sDescBreve,String sDescCompleta,String sCodAlfandegario)
    {
        this.m_oMateriaPrima.setID(sID);
        this.m_oMateriaPrima.setDescricaoBreve(sDescBreve);
        this.m_oMateriaPrima.setDescricaoCompleta(sDescCompleta);
        this.m_oMateriaPrima.setCodigoAlfandegario(sCodAlfandegario);
    }
    
    public boolean registaMateriaPrima()
    {
        return this.m_oEmpresa.registaMateriaPrima(this.m_oMateriaPrima);
    }

    public String getMateriaPrimaAsString()
    {
        return this.m_oMateriaPrima.toString();
    }
}
