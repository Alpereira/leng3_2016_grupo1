/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import model.Empresa;
import model.LocalProducao;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EspecificarLocalProducaoController
{
    private final Empresa m_oEmpresa;
    private LocalProducao m_oLocalProducao;
    public EspecificarLocalProducaoController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public void novoLocalProducao()
    {
        this.m_oLocalProducao = this.m_oEmpresa.novoLocalProducao();
    }
    
    public void setDados(int ID, String sDenominacao,String sAbreviatura,String sEndereco,String sCoordGPS)
    {
        this.m_oLocalProducao.setID(ID);
        this.m_oLocalProducao.setDenominacao(sDenominacao);
        this.m_oLocalProducao.setAbreviatura(sAbreviatura);
        this.m_oLocalProducao.setEndereco(sEndereco);
        this.m_oLocalProducao.setCoordGPS(sCoordGPS);
    }
    
    public boolean registaLocalProducao()
    {
        return this.m_oEmpresa.registaLocalProducao(this.m_oLocalProducao);
    }

    public String getLocalProducaoAsString()
    {
        return this.m_oLocalProducao.toString();
    }
    
}
