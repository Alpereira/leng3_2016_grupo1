/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import controller.EfetuarPrevisaoProducaoController;
import java.util.List;
import model.Empresa;
import model.LocalProducao;
import model.MateriaPrima;
import model.PeriodoColheita;
import utils.Utils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EfetuarPrevisaoProducaoUI
{
    private final Empresa m_oEmpresa;
    private final EfetuarPrevisaoProducaoController m_controller;

    public EfetuarPrevisaoProducaoUI( Empresa oEmpresa )
    {
        this.m_oEmpresa = oEmpresa;
        m_controller = new EfetuarPrevisaoProducaoController(oEmpresa);
    }

    public void run()
    {
        boolean bNew = false;
        List<LocalProducao> llp = m_controller.getLocaisProducao("");

        if (llp.size() > 0)
        {
            LocalProducao lp = (LocalProducao) Utils.apresentaESeleciona(llp, "Selecione o Local de Produção:");
           
            if (lp!= null)
            {
                
                List<MateriaPrima> lmp = m_controller.getMateriaPrimasLocProd(lp);
                
                if (lmp.size() > 0)
                {
                    do
                    {
                        bNew = false;
                        MateriaPrima mp = (MateriaPrima) Utils.apresentaESeleciona(lmp, "Selecione a Matéria-Prima:");

                        if (mp!= null)
                        {
                            do
                            {
                                bNew = false;
                                List<PeriodoColheita> lpc = m_controller.getPeriodosColheitaSemPrevisao(mp);

                                if (lpc.size() > 0)
                                {

                                    PeriodoColheita pc = (PeriodoColheita) Utils.apresentaESeleciona(lpc, "Selecione o Periodo Colheita:");

                                    if (pc!= null)
                                    {
                                       double qtd = Utils.DoubleFromConsole("Introduza a Quantidade Prevista:");
                                       m_controller.addPrevisao(pc, qtd);
                                       bNew = Utils.confirma("Deseja inserir outra previsão para a mesma matéria-prima (S/N)?");
                                    }
                                }
                            }while(bNew);
                            bNew = Utils.confirma("Deseja inserir previsões para outra  matéria-prima (S/N)?");
                        }
                    }while(bNew);
                }
                
            }
        }
        else
            System.out.println("Não existem matérias-primas definidas.");
    }
}
