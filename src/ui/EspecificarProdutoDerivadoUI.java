/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import controller.EspecificarProdutoDerivadoController;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EspecificarProdutoDerivadoUI
{
    private final Empresa m_oEmpresa;
    private final EspecificarProdutoDerivadoController m_controller;

    public EspecificarProdutoDerivadoUI( Empresa oEmpresa )
    {
        this.m_oEmpresa = oEmpresa;
        m_controller = new EspecificarProdutoDerivadoController(oEmpresa);
    }

    public void run()
    {
        System.out.println("\nNovo Produto Derivado:");
        m_controller.novoProdutoDerivado();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Produto Derivado? (S/N)")) 
        {
            if (m_controller.registaProdutoDerivado()) {
                System.out.println("Produto Derivado registado.");
            } else {
                System.out.println("Produto Derivado não registado.");
            }
        }
    }
    
    private void introduzDados() {
        String sID = Utils.readLineFromConsole("Introduza o ID:");
        String sDescBreve = Utils.readLineFromConsole("Introduza Descrição Breve: ");
        String sDescCompleta = Utils.readLineFromConsole("Introduza Descrição Completa: ");
        String sCodAlfa = Utils.readLineFromConsole("Código Alfandegário: ");
       
        m_controller.setDados(sID,sDescBreve,sDescCompleta,sCodAlfa);
    }
    
    private void apresentaDados() 
    {
        System.out.println("\nProduto Derivado:\n" + m_controller.getProdutoDerivadoAsString());
    }
}
