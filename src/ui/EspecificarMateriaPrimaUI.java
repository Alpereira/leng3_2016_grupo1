/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import controller.EspecificarMateriaPrimaController;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EspecificarMateriaPrimaUI
{
    private final Empresa m_oEmpresa;
    private final EspecificarMateriaPrimaController m_controller;

    public EspecificarMateriaPrimaUI( Empresa oEmpresa )
    {
        this.m_oEmpresa = oEmpresa;
        m_controller = new EspecificarMateriaPrimaController(oEmpresa);
    }

    public void run()
    {
        System.out.println("\nNova Matéria-Prima:");
        m_controller.novaMateriaPrima();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados da Matéria-Prima? (S/N)")) 
        {
            if (m_controller.registaMateriaPrima()) {
                System.out.println("Matéria-Prima registada.");
            } else {
                System.out.println("Matéria-Prima não registada.");
            }
        }
    }
    
    private void introduzDados() {
        String sID = Utils.readLineFromConsole("Introduza o ID:");
        String sDescBreve = Utils.readLineFromConsole("Introduza Descrição Breve: ");
        String sDescCompleta = Utils.readLineFromConsole("Introduza Descrição Completa: ");
        String sCodAlfa = Utils.readLineFromConsole("Código Alfandegário: ");
       
        m_controller.setDados(sID,sDescBreve,sDescCompleta,sCodAlfa);
    }
    
    private void apresentaDados() 
    {
        System.out.println("\nMatéria-Prima:\n" + m_controller.getMateriaPrimaAsString());
    }
}
