/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import controller.DefinirPeriodosColheitaController;
import java.util.Date;
import java.util.List;
import model.Empresa;
import model.MateriaPrima;
import utils.Utils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DefinirPeriodosColheitaUI
{
    private final Empresa m_oEmpresa;
    private final DefinirPeriodosColheitaController m_controller;

    public DefinirPeriodosColheitaUI( Empresa oEmpresa )
    {
        this.m_oEmpresa = oEmpresa;
        m_controller = new DefinirPeriodosColheitaController(oEmpresa);
    }

    public void run()
    {
        List<MateriaPrima> lmp = m_controller.getListaMateriasPrimas();

        if (lmp.size() > 0)
        {
            MateriaPrima mp = (MateriaPrima) Utils.apresentaESeleciona(lmp, "Selecione a Matéria-Prima:");
           
            if (mp!= null)
            {
                m_controller.setMateriaPrima(mp);
                boolean bNew = false;
                do
                {
                    Date dtIni = Utils.readDateFromConsole("Data de Inicio:");
                    Date dtFim = Utils.readDateFromConsole("Data de Fim:");

                    if (m_controller.addPeriodoColheita(dtIni, dtFim)) {
                        System.out.println("Periodo de Colheita registado.");
                    } else {
                        System.out.println("Periodo de Colheita não registado.");
                    }

                    bNew = Utils.confirma("Inserir outro periodo de colheita para a mesma matéria-prima (S/N)?");

                }while(bNew);
            }
        }
        else
            System.out.println("Não existem matérias-primas definidas.");
    }
   
}
