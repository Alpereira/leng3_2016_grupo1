/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import java.io.IOException;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class MenuUI
{
    private Empresa m_empresa;
    private String opcao;

    public MenuUI(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public void run() throws IOException
    {
        do
        {
            //opcao = "1";
            System.out.println("\n\n");
            System.out.println("1. Especificar Local de Produção");
            System.out.println("2. Especificar Matéria-Prima");
            System.out.println("3. Definir Produto Derivado");
            System.out.println("4. Definir Periodos de Colheita");
            System.out.println("5. Definir Serviço");
            System.out.println("6. Efetuar Previsão de Produção");
            
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if( opcao.equals("1") )
            {
                EspecificarLocalProducaoUI ui = new EspecificarLocalProducaoUI(m_empresa);
                ui.run();
            }
            if( opcao.equals("2") )
            {
                EspecificarMateriaPrimaUI ui = new EspecificarMateriaPrimaUI(m_empresa);
                ui.run();
            }
            if( opcao.equals("3") )
            {
                EspecificarProdutoDerivadoUI ui = new EspecificarProdutoDerivadoUI(m_empresa);
                ui.run();
            }
            
            if( opcao.equals("4") )
            {
                DefinirPeriodosColheitaUI ui = new DefinirPeriodosColheitaUI(m_empresa);
                ui.run();
            }
            
            if( opcao.equals("5") )
            {
                DefinirServicoUI ui = new DefinirServicoUI(m_empresa);
                ui.run();
            }
            
            if( opcao.equals("6") )
            {
                EfetuarPrevisaoProducaoUI ui = new EfetuarPrevisaoProducaoUI(m_empresa);
                ui.run();
            }

            // Incluir as restantes opções aqui
            
        }
        while (!opcao.equals("0") );
    }
}
