/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import controller.DefinirServicoController;
import java.util.ArrayList;
import java.util.List;
import model.Empresa;
import model.LocalProducao;
import model.MateriaPrima;
import model.OutroLocal;
import model.Produto;
import model.ProdutoDerivado;
import model.RegistoDisponibilizacao;
import model.RegistoOutroLocal;
import model.RegistoPotencialDisponibilizacao;
import model.RegistoProduto;
import model.RegistoServico;
import utils.Utils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DefinirServicoUI
{
    private final Empresa m_oEmpresa;
    private final RegistoServico m_oRegistoServico;
    private final RegistoProduto m_oRegistoProduto;
    private final RegistoOutroLocal m_oRegistoOutroLocal;
    private final RegistoDisponibilizacao m_oRegistoDisponibilizacao;
    private final RegistoPotencialDisponibilizacao m_oRegistoPotencialDisponibilizacao;
    private final DefinirServicoController m_controller;

    public DefinirServicoUI( Empresa oEmpresa, RegistoServico oRegistoServico,RegistoProduto oRegistoProduto,RegistoOutroLocal oRegistoOutroLocal,RegistoDisponibilizacao oRegistoDisponibilizacao, RegistoPotencialDisponibilizacao oRegistoPotencialDisponibilizacao )
    {
        this.m_oEmpresa = oEmpresa;
        this.m_oRegistoOutroLocal = oRegistoOutroLocal;
        this.m_oRegistoProduto= oRegistoProduto;
        this.m_oRegistoServico= oRegistoServico;
        this.m_oRegistoDisponibilizacao= oRegistoDisponibilizacao;
        this.m_oRegistoPotencialDisponibilizacao= oRegistoPotencialDisponibilizacao;
        m_controller = new DefinirServicoController(oEmpresa, oRegistoOutroLocal, oRegistoServico, oRegistoProduto, oRegistoDisponibilizacao,oRegistoPotencialDisponibilizacao);
    }

    public void run()
    {
        System.out.println("\nNovo Serviço:");
        m_controller.novoServico();

        List<Produto> lstProds = introduzDados();
        List<MateriaPrima> lstMP = getMateriasPrimas(lstProds);
        List<ProdutoDerivado> lstPD = getProdutosDerivados(lstProds);
        List<OutroLocal> lstOL= getOutrosLocais(lstProds);
        
        if (lstMP.size() > 0)
        {
            boolean bNew = true;
            do
            {
//                OutroLocal oOL = (OutroLocal) Utils.apresentaESeleciona(lstOL, "Selecione pelos menos dois locais");
//                if (oOL != null){
                MateriaPrima oMP = (MateriaPrima) Utils.apresentaESeleciona(lstMP, "Selecione a Matéria-Prima:");
                if (oMP != null)
                {
                    ProdutoDerivado oPD = (ProdutoDerivado) Utils.apresentaESeleciona (lstPD, "Selecione o Produto Derivado:");
                    if (oPD != null)
                    {
                    Produto oProd = (Produto) Utils.apresentaESeleciona(lstProds, "Selecione o Produto Resultante:");
                    if (oProd != null)
                    {
                        double dRacio = Utils.DoubleFromConsole("Indique o rácio");
                        
                        m_controller.setMateriaPrima(oMP);
                        m_controller.setProdutoResultante(oProd);
                        m_controller.setRacio(dRacio);
                    }
                }
                }
                
                bNew = Utils.confirma("Deseja inserir outra aplicação do serviço (S/N?)");
            }while(bNew);
        }
        else
            System.out.println("Não é possivel especificar Aplicações porque nao oexistem Matérias-Primas definidas.");
        
        apresentaDados();

        if (Utils.confirma("Confirma os dados do Serviço? (S/N)")) 
        {
            if (m_controller.registaServico()) {
                System.out.println("Serviço registado.");
            } else {
                System.out.println("Serviço não registado.");
            }
        }
    }
    
    private List<Produto> introduzDados() {
        String sCod = Utils.readLineFromConsole("Introduza o Código:");
        String sDescricao = Utils.readLineFromConsole("Introduza a Descrição: ");
        String sFxTec = Utils.readLineFromConsole("Introduza a Ficha Técnica: ");
        String sLocEsp= Utils.readLineFromConsole("Introduza o Local de aplicação do serviço: "); 
        String sCusto= Utils.readLineFromConsole("Introduza o custo fixo do servico: ");
        String sCapacidade= Utils.readLineFromConsole("Introduza a capacidade nominal deste servico: ");
        String sCustoAbertura= Utils.readLineFromConsole("Introduza o custo de abertura deste servico: ");
       
        return m_controller.setDados(sCod,sDescricao,sFxTec,sLocEsp,sCusto, sCapacidade, sCustoAbertura);
    }
    
    private void apresentaDados() 
    {
        System.out.println("\nServiço:\n" + m_controller.getServicoAsString());
    }
    
    private List<MateriaPrima> getMateriasPrimas(List<Produto> lstProds)
    {
        List<MateriaPrima> lstMP = new ArrayList<>();
        for (Produto p: lstProds)
        {
            if (p instanceof MateriaPrima)
                lstMP.add((MateriaPrima)p);
        }
        return lstMP;
    }
    private List<OutroLocal> getOutrosLocais(List<Produto> lstProds)
    {
        List<OutroLocal> lstOL = new ArrayList<>();
        return lstOL;
    }
    
    private List<ProdutoDerivado> getProdutosDerivados(List<Produto> lstProds)
    {
        List<ProdutoDerivado> lstPD = new ArrayList<>();
        for (Produto p: lstProds)
        {
            if (p instanceof ProdutoDerivado)
                lstPD.add((ProdutoDerivado)p);
        }
            return lstPD;
    }
}