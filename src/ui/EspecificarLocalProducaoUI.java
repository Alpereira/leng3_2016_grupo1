/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import controller.EspecificarLocalProducaoController;
import model.Empresa;
import utils.Utils;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EspecificarLocalProducaoUI
{
    private final Empresa m_oEmpresa;
    private final EspecificarLocalProducaoController m_controller;

    public EspecificarLocalProducaoUI( Empresa oEmpresa )
    {
        this.m_oEmpresa = oEmpresa;
        m_controller = new EspecificarLocalProducaoController(oEmpresa);
    }

    public void run()
    {
        System.out.println("\nNovo Local de Produção:");
        m_controller.novoLocalProducao();

        introduzDados();

        apresentaDados();

        if (Utils.confirma("Confirma os dados do Local de Produção? (S/N)")) 
        {
            if (m_controller.registaLocalProducao()) {
                System.out.println("Local de Produção registado.");
            } else {
                System.out.println("Local de Produção não registado.");
            }
        }
    }
    
    private void introduzDados() {
        int ID = Utils.IntFromConsole("Introduza o ID:");
        String sDenominacao = Utils.readLineFromConsole("Introduza Denominação: ");
        String sAbreviatura = Utils.readLineFromConsole("Introduza Abreviatura: ");
        String sEndereco = Utils.readLineFromConsole("Introduza Endereço: ");
        String sCoordGPS = Utils.readLineFromConsole("Introduza Coordenadas GPS: ");
       
        m_controller.setDados(ID,sDenominacao,sAbreviatura,sEndereco,sCoordGPS);
    }
    
    private void apresentaDados() 
    {
        System.out.println("\nLocal de Produção:\n" + m_controller.getLocalProducaoAsString());
    }
 
}
